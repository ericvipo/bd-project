import axios from 'axios'

const apiClient = axios.create({
  baseURL: process.env.VUE_APP_API_URI,
  withCredentials: false, //This is the default
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
  // timeout: 10000
})

// apiClient.interceptors.request.use(
//   config =>
//     new Promise(resolve => {
//       setTimeout(() => {
//         console.log('GAAAA')
//         config.headers.async = 'promise'
//         resolve(config)
//       }, 3000)
//     })
// )

export default {
  //proyectos
  getProjects() {
    return apiClient.get('/projects')
  },

  //rutas
  getRuta(pyto) {
    return apiClient.get(`/route/${pyto}`)
  },

  //tramos
  getTramos(ruta) {
    return apiClient.get(`/tramos/${ruta}`)
  },
  postTramo({ route, data }) {
    console.log(data.get('duracion'))
    return apiClient.post(`/tramo/${route}`, data)
  },
  putTramo({ id, data }) {
    return apiClient.put(`/tramo/${id}`, data)
  },
  deleteTramo({ id, videoid }) {
    return apiClient.delete(`/tramo/${id}?videoid=${videoid}`)
  }
}
