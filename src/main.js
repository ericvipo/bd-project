import Vue from 'vue'
import App from './App.vue'
import store from './store'

// modulos para importacion masiva
import upperFirst from 'lodash/upperFirst'
import camelCase from 'lodash/camelCase'

// inicio de importacion masiva
const requireComponent = require.context(
  './components',
  false, // Whether or not to look in subfolders
  /Base[A-Z]\w+\.(vue|js)$/
)

requireComponent.keys().forEach(fileName => {
  const componentConfig = requireComponent(fileName)

  const componentName = upperFirst(
    camelCase(
      fileName
        .split('/')
        .pop()
        .replace(/\.\w+$/, '')
    )
  )

  Vue.component(componentName, componentConfig.default || componentConfig)
})
// fin de importacion masiva

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  store
}).$mount('#app')
