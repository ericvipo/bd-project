import api from '@/services/api'

export const namespaced = true

export const state = {
  ruta: []
}

export const getters = {
  // getRandomLeaders(state) {
  // return shuffle(state.leaders)
  // }
}

export const mutations = {
  SET_RUTA(state, ruta) {
    state.ruta = ruta
  }
}

export const actions = {
  fetchRuta({ commit, dispatch }, proy) {
    return api
      .getRuta(proy)
      .then(response => {
        console.log(response.data)
        commit('SET_RUTA', response.data)
        dispatch('tramo/fetchTramos', response.data[0].codruta, { root: true })
      })
      .catch(error => console.log('Error:', error))
  }
}
