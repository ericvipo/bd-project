import api from '@/services/api'

export const namespaced = true

export const state = {
  projects: []
}

export const getters = {
  // getRandomLeaders(state) {
  // return shuffle(state.leaders)
  // }
}

export const mutations = {
  SET_PROJECTS(state, projects) {
    state.projects = projects
  }
}

export const actions = {
  fetchProjects({ commit }) {
    return api
      .getProjects()
      .then(response => {
        commit('SET_PROJECTS', response.data)
      })
      .catch(error => console.log('Error:', error))
  }
}
