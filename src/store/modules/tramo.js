import api from '@/services/api'

export const namespaced = true

export const state = {
  tramos: []
}

export const getters = {
  // getRandomLeaders(state) {
  // return shuffle(state.leaders)
  // }
}

export const mutations = {
  // ADD_TRAMO(state, tramo) {
  //   state.tramos.push(tramo)
  // },
  SET_TRAMOS(state, tramos) {
    state.tramos = tramos
  }
}

export const actions = {
  fetchTramos({ commit }, ruta) {
    return api
      .getTramos(ruta)
      .then(response => {
        console.log(response.data)
        commit('SET_TRAMOS', response.data)
      })
      .catch(error => console.log('Error:', error))
  },
  createTramo({ commit, dispatch }, payload) {
    return api.postTramo(payload).then(r => {
      console.log(r, 'tramo creado')
      dispatch('fetchTramos', payload.data.get('codruta'))
      // commit('ADD_TRAMO', r.data)
    })
  },
  updateTramo({ dispatch }, payload) {
    return api.putTramo(payload).then(() => {
      console.log('tramo actualizado')
      dispatch('fetchTramos', payload.data.get('codruta'))
    })
  },
  removeTramo({}, payload) {
    return api.deleteTramo(payload).then(() => {
      console.log('tramo eliminado')
    })
  }
}
