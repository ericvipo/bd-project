import Vue from 'vue'
import Vuex from 'vuex'

import * as project from '@/store/modules/project'
import * as ruta from '@/store/modules/ruta'
import * as tramo from '@/store/modules/tramo'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    project,
    ruta,
    tramo
  }
})
